package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Bundle in = getIntent().getExtras();
        String nim =  in.getString("nim");
        String nama =  in.getString("nama");
        TextView tvNim2 = findViewById(R.id.tvNim2);
        TextView tvNama2 = findViewById(R.id.tvNama2);
        tvNim2.setText(nim);
        tvNama2.setText(nama);
    }
}